# This is a very simple django project with mongodb database config.
Everything is almost same to the regular django project except the database config in settings.py


I choosed `djongo` to setup the django project with mongo. There are also another tools avaiable to connect mongodb with django like `mongoengine`.
With `djongo` we can use the same django builtin query statement what we used in relational database (as far as I learned).
